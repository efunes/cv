
let fill_tecnico = (min, max) => {
  let ret = []
  let rand = min + Math.round(Math.random() * (max - min))
  for (let i = 0; i < rand; i++) {
    ret.push({
      orden: i + 1,
      titulo: "Item",
      detalles: "Experiencia"
    })
  }
  return ret;
}

let fill_laboral = (min, max) => {
  let ret = []
  let rand = min + Math.round(Math.random() * (max - min))
  for (let i = 0; i < rand; i++) {
    ret.push({
      orden: i + 1,
      lugar: "Lugar",
      periodo: "desde - hasta",
      actividades: ["Actividad A.", "Actividad B", "Actividad C"],
      contacto: "Superior 1 (0985) 123 456"
    })
  }
  return ret;
}

const state = {
  personales: {
    nombre: "Nombre",
    apellido: "Apellido",
    about_me: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec porttitor, tellus quis tincidunt tincidunt, lacus enim interdum risus, in euismod magna quam nec est. Fusce a lorem ac ipsum aliquam ultricies eget eget orci. Nullam elementum neque ac consectetur mattis. Nullam maximus est neque, nec condimentum leo scelerisque ac. Suspendisse et magna auctor, aliquet tellus nec, ultrices libero. Vestibulum scelerisque enim massa, ut pellentesque leo accumsan nec.",
    datos: [
      { orden: 1, titulo: "Fecha de Nacimiento", valor: "01/01/1970" },
      { orden: 2, titulo: "C.I.N°", valor: "1.234.567" },
      { orden: 3, titulo: "Dirección", valor: "Evergreen N° 742" },
      { orden: 4, titulo: "Ciudad", valor: "Springfield" },
      { orden: 5, titulo: "Teléfono", valor: "555-636-452 265" },
      { orden: 6, titulo: "E-mail", valor: "lorem.impsum.dolor@email.com" },
    ],
    educacion: [
      { orden: 1, detalles: "Terciaria: Álma Máter\nCarrera: Carrera\nFecha de titulación/Progreso" },
      { orden: 2, detalles: "Secundaria: Álma Máter\nÁrea de Estudio: Bachiller\nFecha de finalización"},
      { orden: 3, detalles: "Primaria: Año de Finalización" },
    ],
    nombre_completo: "Apellido, Nombre",
  },
  tecnicos: {
    principales: fill_tecnico(3, 7),
    secundarios: fill_tecnico(3, 7),
    extras: fill_tecnico(1, 3),
  },
  laborales: {
    experiencia: fill_laboral(2, 4)
  }
};

const getters = {};

const actions = {

};

const mutations = {

};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
};
