import Vue from "vue";
import Router from "vue-router";
import land from "../views/land"

Vue.use(Router);

var router = new Router({
    mode: 'history',
    routes: [
        {
            path: "/",
            name: "land",
            component: land,
            meta: {}
        },
    ]
});

export default router;
