require('./bootstrap');

import Vue from "vue";
import App from "./App.vue";
import store from "./store";
import router from "./router";
import VueParallaxJs from "vue-parallax-js";

Vue.use(VueParallaxJs);

const app = new Vue({
    store,
    router,
    render: h => h(App)
});

app.$mount("#app");